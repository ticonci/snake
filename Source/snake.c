#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>
#include <windows.h>
#include <stdbool.h>
#include <mmsystem.h>
#pragma comment(lib, "winmm.lib")

#include "border.h"

//欢迎界面
void FirstPage(void);
//播放音乐
void MyPlaySound(void);
//检测空格
void detectSpace(void);
//播放音乐停止
void stopMusic(void);
//画背景
void ShowBackground(void);
//为蛇产生一个随机的位置
void SetSnakeRandPos();
//画蛇
void DrawSnake(void);
//蛇动
void SnakeMove(void);
//销毁蛇
void DropSnake(void);
//改变蛇的方向
void ChangeDir(void);
//蛇死亡判断
bool IsSnakeDead(void);
//产生食物
void ProduceFood(void);
//蛇变成
void SnakeGrowUp(void);

int main(void)
{

    FirstPage();
    MyPlaySound();

    detectSpace();

    //停止播放所有声音
    stopMusic();
    system("cls");
    //为蛇产生随机位置
    SetSnakeRandPos();
    ShowBackground();


    while(1)
    {
        system("cls");
        //产生食物
        ProduceFood();
        SnakeGrowUp();
        //改变方向
        ChangeDir();
        SnakeMove();
        ShowBackground();

        if(false == IsSnakeDead())
        {
            printf("Snake died");
            break;
        }

        Sleep(500);
    }


    system("pause");
    return 0;
}

void MyPlaySound(void)
{
    PlaySound("ifeelclosetoyou.wav", NULL, SND_FILENAME | SND_ASYNC);
}

void stopMusic(void)
{
    PlaySound(NULL, 0, 0);
}

void FirstPage(void)
{
    printf("\n\n\n\n");
    printf("\t\twelcome to the world of rentro snaker\n");
    printf("\t\t\tPress [SPACE] to start \n");
    printf("\t\tUSE A/S/D/W to control the snake\n");
}

void detectSpace(void)
{
    char chInput;

    while(1)
    {
        chInput = getch();
        if(' ' == chInput)
        {
            break;
        }
    }
}

//画背景
void ShowBackground(void)
{
    int i = 0;

    system("cls");
    for(i = 0; i < 20; i++)
    {
        printf(g_strGameBack[i]);
    }
}

//为蛇产生一个随机的位置
void SetSnakeRandPos()
{
    int nX = -1;
    int nY = -1;

    //产生随机数
    srand((unsigned int)time(NULL));
    nX = rand() % 15 + 5;
    nY = rand() % 14 + 5;

    //初始化蛇3个节点
    g_arrSnake[0][0] = nY; //行
    g_arrSnake[0][1] = nX * 2; //列
    g_arrSnake[0][2] = to_west;

    g_arrSnake[1][0] = nY; //行
    g_arrSnake[1][1] = nX * 2 + 2; //列
    g_arrSnake[1][2] = to_west;

    g_arrSnake[2][0] = nY; //行
    g_arrSnake[2][1] = nX * 2 + 4; //列
    g_arrSnake[2][2] = to_west;

    //画蛇
    DrawSnake();
}

//画蛇
void DrawSnake(void)
{
    int i = 0;
    for(i = 0; g_arrSnake[i][0] != 0; i++)
    {
        strncpy(&g_strGameBack[g_arrSnake[i][0]][g_arrSnake[i][1]], "##", 2);
    }
}

//蛇动
void SnakeMove(void)
{
    int i = SNAKE_LENGTH - 1;

    //销毁蛇
    DropSnake();

    for(i; i >= 1; i--)
    {
        //过滤掉非法蛇身
        if(0 == g_arrSnake[i][1])
        {
            continue;
        }
        //把前一个节点的值，赋值给当前节点
        g_arrSnake[i][0] = g_arrSnake[i - 1][0];
        g_arrSnake[i][1] = g_arrSnake[i - 1][1];
        g_arrSnake[i][2] = g_arrSnake[i - 1][2];
    }

    //处理蛇头
    g_arrSnake[0][2] = g_nSnakeDir;

    if(to_west == g_arrSnake[0][2] || to_east == g_arrSnake[0][2])
    {
        g_arrSnake[0][1] += g_arrSnake[0][2];
    }
    else
    {
        g_arrSnake[0][0] += g_arrSnake[0][2];
    }

    //画蛇
    DrawSnake();
}

//销毁蛇
void DropSnake(void)
{
    int i = 0;
    for(i = 0; g_arrSnake[i][0] != 0; i++)
    {
        strncpy(&g_strGameBack[g_arrSnake[i][0]][g_arrSnake[i][1]], "  ", 2);
    }

}

//改变蛇的方向
void ChangeDir()
{
    if(GetAsyncKeyState('W'))
    {
        if(to_south != g_arrSnake[0][2])
        {
            g_nSnakeDir = to_north;
        }
    }
    else if(GetAsyncKeyState('S'))
    {
        if(to_north != g_arrSnake[0][2])
        {
            g_nSnakeDir = to_south;
        }
    }
    else if(GetAsyncKeyState('A'))
    {
        if(to_east != g_arrSnake[0][2])
        {
            g_nSnakeDir = to_west;
        }
    }
    else if(GetAsyncKeyState('D'))
    {
        if(to_west != g_arrSnake[0][2])
        {
            g_nSnakeDir = to_east;
        }
    }
}

//蛇死亡判断
bool IsSnakeDead(void)
{
    if(to_west == g_arrSnake[0][2] || to_east == g_arrSnake[0][2])
    {
        if(0 == strncmp(&g_strGameBack[g_arrSnake[0][0]][g_arrSnake[0][1] + g_arrSnake[0][2]], "##", 2))
        {
            return false;
        }
    }
    else
    {
        if(0 == strncmp(&g_strGameBack[g_arrSnake[0][0] + g_arrSnake[0][2]][g_arrSnake[0][1]], "##", 2))
        {
            return false;
        }
    }

    return true;
}

//产生食物
void ProduceFood(void)
{
    //产生随机坐标

    int i = 0;
    //判断是否产生新的事物
    if(false == g_bIsProFood)
    {
        return;
    }

    srand((unsigned int)time(NULL));

    while(1)
    {
        bool bFlag = true;

        g_nLine= rand() % 18 + 1;
        g_nLie = rand() % 20 + 1;

        for(i = 0; g_arrSnake[i][0] != 0; i++)
        {
            if(g_nLine == g_arrSnake[i][0] && g_nLie == g_arrSnake[i][1])
            {
                bFlag = false;
                break;
            }
        }

        if(true == bFlag)
        {
            break;
        }
    }

    //坐标画食物
    strncpy(&g_strGameBack[g_nLine][g_nLie * 2], "@@", 2);
    g_bIsProFood = false;
}

//蛇变成
void SnakeGrowUp(void)
{
    //舌头坐标跟事物坐标相等
    if(g_nLine == g_arrSnake[0][0] && g_nLie * 2 == g_arrSnake[0][1])
    {
        //蛇长大
        if(to_east == g_arrSnake[g_SnakeLength][2])
        {
            g_arrSnake[g_SnakeLength + 1][0] = g_arrSnake[g_SnakeLength][0];
            g_arrSnake[g_SnakeLength + 1][1] = g_arrSnake[g_SnakeLength][0] - 2;
            g_arrSnake[g_SnakeLength + 1][2] = g_arrSnake[g_SnakeLength][0];
        }
        else if(to_west == g_arrSnake[g_SnakeLength][2])
        {
            g_arrSnake[g_SnakeLength + 1][0] = g_arrSnake[g_SnakeLength][0];
            g_arrSnake[g_SnakeLength + 1][1] = g_arrSnake[g_SnakeLength][0] + 2;
            g_arrSnake[g_SnakeLength + 1][2] = g_arrSnake[g_SnakeLength][0];
        }
        else if(to_north == g_arrSnake[g_SnakeLength][2])
        {
            g_arrSnake[g_SnakeLength + 1][0] = g_arrSnake[g_SnakeLength][0] + 1;
            g_arrSnake[g_SnakeLength + 1][1] = g_arrSnake[g_SnakeLength][0];
            g_arrSnake[g_SnakeLength + 1][2] = g_arrSnake[g_SnakeLength][0];
        }
        else if(to_south == g_arrSnake[g_SnakeLength][2])
        {
            g_arrSnake[g_SnakeLength + 1][0] = g_arrSnake[g_SnakeLength][0] - 1;
            g_arrSnake[g_SnakeLength + 1][1] = g_arrSnake[g_SnakeLength][0];
            g_arrSnake[g_SnakeLength + 1][2] = g_arrSnake[g_SnakeLength][0];
        }

        g_SnakeLength++;
        g_bIsProFood = true;
    }
}
