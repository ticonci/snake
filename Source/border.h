#ifndef BORDER_H_INCLUDED
#define BORDER_H_INCLUDED



#endif // BORDER_H_INCLUDED

#define SNAKE_LENGTH 20

char g_strGameBack[20][48] = {"##############################################\n",
                              "##                                          ##\n",
                              "##                                          ##\n",
                              "##                                          ##\n",
                              "##                                          ##\n",
                              "##                                          ##\n",
                              "##                                          ##\n",
                              "##                                          ##\n",
                              "##                                          ##\n",
                              "##                                          ##\n",
                              "##                                          ##\n",
                              "##                                          ##\n",
                              "##                                          ##\n",
                              "##                                          ##\n",
                              "##                                          ##\n",
                              "##                                          ##\n",
                              "##                                          ##\n",
                              "##                                          ##\n",
                              "##                                          ##\n",
                              "##############################################\n"

};


//方向
enum {to_east = 2, to_west = -2, to_north = -1, to_south = 1};
//蛇的长度
int g_SnakeLength = 2;
//蛇数组
int g_arrSnake[SNAKE_LENGTH][3] = {0};
//蛇的方向
int g_nSnakeDir = to_west;
//
int g_bIsProFood = true;
//随机左边
int g_nLine;
int g_nLie;
